package com.example.jeanclaude.test_room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Word.class}, version = 1)
public abstract class WordRoomDatabase extends RoomDatabase {

    private static volatile WordRoomDatabase INSTANCE;

    static WordRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (WordRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WordRoomDatabase.class, "word_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {
                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final WordDao mDao;

        PopulateDbAsync(WordRoomDatabase db) {
            mDao = db.wordDao();
        }

        // modification du code pour montrer que Room garde bien les données
        // Attention ! code très basique : il ne faut pas que la base contienne
        // déjà un de ces mots, sinon crash !
        // Pour éviter cela, on modifie la stratégie de @Insert pour faire un REPLACE par exemple.

        @Override
        protected Void doInBackground(final Void... params) {
            if (mDao.getAllWords() != null) {
                Word word = new Word("Hello");
                mDao.insert(word);
                word = new Word("World");
                mDao.insert(word);
                word = new Word("Ceci");
                mDao.insert(word);
                word = new Word("est");
                mDao.insert(word);
                word = new Word("une");
                mDao.insert(word);
                word = new Word("première");
                mDao.insert(word);
                word = new Word("application");
                mDao.insert(word);
                word = new Word("simple");
                mDao.insert(word);
                word = new Word("avec");
                mDao.insert(word);
                word = new Word("Room");
                mDao.insert(word);
            } else {
                mDao.deleteAll();
                Word word = new Word("Hello");
                mDao.insert(word);
                word = new Word("World");
                mDao.insert(word);
            }
            return null;
        }
    }

    public abstract WordDao wordDao();
}
